//
//  detailedExperience_ViewController.swift
//  Gustaf_Levinsson_CV
//
//  Created by Gustaf on 2018-10-31.
//  Copyright © 2018 Gustaf_Levinsson. All rights reserved.
//

import UIKit

class detailedExperience_ViewController: UIViewController {

    var experienceInfo: experience!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var workImage: UIImageView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareInformation(exp: experienceInfo)

    }
    
    func prepareInformation(exp: experience){
        self.title = exp.workplace
        self.titleLabel.text = exp.title
        self.descriptionLabel.text = exp.description
        self.workImage.image = UIImage(named: exp.imageName)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
