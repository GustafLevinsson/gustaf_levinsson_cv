//
//  ViewController.swift
//  Gustaf_Levinsson_CV
//
//  Created by Gustaf on 2018-10-30.
//  Copyright © 2018 Gustaf_Levinsson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var profilePicture: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        profilePicture.roundImage()
        // Do any additional setup after loading the view, typically from a nib.
    }


    @IBAction func ShowSkillsButtonClicked(_ sender: Any) {
        performSegue(withIdentifier: "ShowSkills", sender: self)
    }
    @IBAction func ShowErfarenheterButtonClicked(_ sender: Any) {
        performSegue(withIdentifier: "ShowExperience", sender: self)
    }
}

