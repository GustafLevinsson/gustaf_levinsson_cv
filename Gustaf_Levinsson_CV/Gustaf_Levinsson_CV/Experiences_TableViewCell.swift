//
//  Experiences_TableViewCell.swift
//  Gustaf_Levinsson_CV
//
//  Created by Gustaf on 2018-11-02.
//  Copyright © 2018 Gustaf_Levinsson. All rights reserved.
//

import UIKit

class Experiences_TableViewCell: UITableViewCell {

    @IBOutlet weak var placeImage: UIImageView!
    
    @IBOutlet weak var placeTitleLabel: UILabel!
    
    @IBOutlet weak var positionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
