//
//  skills_ViewController.swift
//  Gustaf_Levinsson_CV
//
//  Created by Gustaf on 2018-11-07.
//  Copyright © 2018 Gustaf_Levinsson. All rights reserved.
//

import UIKit

class skills_ViewController: UIViewController {
    
    var isMoving = false
    var flipCount: Int = 0
    
    @IBOutlet weak var gotchaLabel: UILabel!
    @IBOutlet weak var animationView: UIView!
    @IBOutlet weak var button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Skills"
        gotchaLabel.isHidden = true
    }
    
    @IBAction func flipButton(_ sender: Any) {
        doMagicTrick()
        switch flipCount {
        case 0:
            button.setTitle("Again", for: .normal)
            break
        case 1:
            button.setTitle("Yay! One more time!", for: .normal)
            break
        case 2:
            button.setTitle("Please, just once more!", for: .normal)
            break
        default:
            button.setTitle("Again", for: .normal)

        }
    }
    
    func doMagicTrick(){
        if flipCount < 2 {
            flipCount += 1
            let randNumber = Int.random(in: 1...5)
            switch randNumber {
            case 1:
                UIView.transition(with: animationView, duration: 2, options: .transitionFlipFromLeft, animations: {print("1")}, completion: nil)
                break
            case 2:
                UIView.transition(with: animationView, duration: 2, options: .transitionFlipFromTop, animations: {print("2")}, completion: nil)
                break
            case 3:
                UIView.transition(with: animationView, duration: 2, options: .transitionFlipFromTop, animations: {print("3")}, completion: nil)
                break
            case 4:
                UIView.transition(with: animationView, duration: 2, options: .transitionCurlDown, animations: {print("4")}, completion: nil)
                break
            case 5:
                UIView.transition(with: animationView, duration: 2, options: .transitionFlipFromBottom, animations: {print("5")}, completion: nil)
                break
            default:
                print("Failed")
            }
        }
        else {
            if !isMoving {
                isMoving = true
                UIView.animate(withDuration: 5) {
                    self.animationView.center = CGPoint(x: 1000, y: 1000)
                    self.navigationItem.setHidesBackButton(true, animated: true)

                }
                UILabel.animate(withDuration: 10) {
                    self.gotchaLabel.isHidden = false
                }
            }

        }
        
    }
}
