//
//  extensions.swift
//  Gustaf_Levinsson_CV
//
//  Created by Gustaf on 2018-11-01.
//  Copyright © 2018 Gustaf_Levinsson. All rights reserved.
//

import UIKit

extension UIColor {
    static let myKindaRed = UIColor(named: "redish")
}

extension UIImageView {
    func roundImage () {
        self.layer.cornerRadius = self.frame.width / 2
        self.layer.masksToBounds = true
    }
}

