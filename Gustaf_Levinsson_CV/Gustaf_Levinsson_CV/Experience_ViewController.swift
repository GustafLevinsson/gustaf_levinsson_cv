//
//  Experience_ViewController.swift
//  Gustaf_Levinsson_CV
//
//  Created by Gustaf on 2018-11-01.
//  Copyright © 2018 Gustaf_Levinsson. All rights reserved.
//

import UIKit

class Experience_ViewController: UIViewController {
    
    @IBOutlet weak var experienceTableView: UITableView!
    var experiences: [experience]!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Erfarenheter"
        experiences = fillExperienceArray()

        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? detailedExperience_ViewController{
            if let indexPath = sender as? IndexPath {
                destination.experienceInfo = experiences[indexPath.row]
                print("Going to cell \(experiences[indexPath.row].workplace)")


            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension Experience_ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return experiences.count
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ExperienceCell", for: indexPath) as? Experiences_TableViewCell{
            let workplace = experiences[indexPath.row]
            cell.placeImage.image = UIImage(named: "\(workplace.imageName)")
            cell.placeImage.roundImage()
            cell.placeTitleLabel.text = workplace.workplace
            cell.positionLabel.text = workplace.title
            return cell
            
        }
        else{
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showExperienceDetails", sender: indexPath)
        print("Going to cell now")

    }
    
}

func fillExperienceArray()-> [experience]{
    let tempArray: [experience] = [
        experience(workplace: "Regementet",
                   years: "2018",
                   title: "Säljare",
                   description: "Säljare på Regementet som är en butik inne på asecs köpcentrum i Jönköping.",
                   typeOfExperience: "Work",
                   imageName: "regementet"),
        experience(workplace: "Byggmax",
                   years: "2017-2018",
                   title: "Säljare",
                   description: "Säljare på Byggmax i Jönköping. En bygghandel där jag bland annat mätte virke, tog betalt och hade övergripande ansvar inne i butiken.",
                   typeOfExperience: "Work",
                   imageName: "byggmax"),
        experience(workplace: "Jönköpings Kommun",
                   years: "2015-2018",
                   title: "Vikarie",
                   description: "Jobbade heltid under 2015 som elevassistent på Landsjöskolan i Jönköping. Sedan har jag arbetat som vikare till och från.",
                   typeOfExperience: "Work",
                   imageName: "jonkoping"),
        experience(workplace: "Jönköping University",
                   years: "2016-present",
                   title: "Mjukvaruutveckling & Mobila plattformar",
                   description: "Studerar nu till mjukvaruutvecklare på Jönköping University.",
                   typeOfExperience: "school",
                   imageName: "JU"),
        experience(workplace: "Södra Vätterbygdens Folkhögskola",
                   years: "2013-2014",
                   title: "CollegeLinjen",
                   description: "Gick på folkhögskola där vi reste mycket och lärde oss mycket engelska och historia. Sedan utlandsstudier i Chicago under en termin.",
                   typeOfExperience: "school",
                   imageName: "svf"),
        experience(workplace: "John Bauer Gymnasiet",
                   years: "2009-2012",
                   title: "IT-programmet",
                   description: "Gymnasieutbildning med programmering och liknande",
                   typeOfExperience: "school",
                   imageName: "johnbauer"),
        experience(workplace: "Jönköpings Kommun",
                   years: "2016-present",
                   title: "Nämndeman",
                   description: "Nämndeman i utbildning och arbetsmarknadsnämnden.",
                   typeOfExperience: "other",
                   imageName: "jonkoping"),
        experience(workplace: "Infospread",
                   years: "2018",
                   title: "Praktik",
                   description: "Praktik på Infospread AB med automatisk testing av deras mobila applikation.",
                   typeOfExperience: "other",
                   imageName: "infospread"),
        experience(workplace: "Jonglera",
                   years: "2006-present",
                   title: "0-3 bollar samtidigt",
                   description: "0-3 bollar är ungefär lika svårt. Försöker ständigt använda mig av denna talang inom programmering, ännu ingen lycka..",
                   typeOfExperience: "other",
                   imageName: "jonglera")
    ]
    return tempArray
    
}
