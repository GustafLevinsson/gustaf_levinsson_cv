//
//  experiences.swift
//  Gustaf_Levinsson_CV
//
//  Created by Gustaf on 2018-11-01.
//  Copyright © 2018 Gustaf_Levinsson. All rights reserved.
//

import Foundation

struct experience {
    var workplace: String
    var years: String
    var title: String
    var description: String
    var typeOfExperience: String
    var imageName: String
    
    init(workplace: String, years: String, title: String, description: String, typeOfExperience: String , imageName: String) {
        self.workplace = workplace
        self.years = years
        self.title = title
        self.description = description
        self.typeOfExperience = typeOfExperience
        self.imageName = imageName
    }
}
